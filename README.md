# DEB package for creating/removing admins on production nodes

## Adding new admin

To add new admin with 'user1' name, write his SSH public key
to ./public-keys/user1 file.

## Removing admin

Made in two necessary stages:

1. Remove appropriate file from ./public-keys directory;
2. Append user name to file 'revoked'.
